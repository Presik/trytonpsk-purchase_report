# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import purchase


def register():
    Pool.register(
        purchase.Purchase,
        purchase.PurchaseLine,
        purchase.PurchaseAnalyticStart,
        module='purchase_report', type_='model')
    Pool.register(
        purchase.PurchaseAnalytic,
        purchase.PurchaseForceCancel,
        module='purchase_report', type_='wizard')
    Pool.register(
        purchase.PurchaseDetailedReport,
        purchase.PurchaseAnalyticReport,
        module='purchase_report', type_='report')
